<?php

use Pneuma\Cgi;
use Pneuma\ErrorHandler;
use Pneuma\Exception\MethodNotAllowedException;
use Pneuma\Exception\NotFoundException;
use Pneuma\Interface\ResponseInterface;

$baseCgi = __DIR__;
$root = dirname($baseCgi);
$baseApp = $root . DIRECTORY_SEPARATOR . 'app';
$environment = $root . DIRECTORY_SEPARATOR . 'environment.ini';
$autoload = $root . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$https = $_SERVER['HTTPS'] ?? 'off';
$scheme = $_SERVER['REQUEST_SCHEME'] ?? 'http';
$protocol = $https == 'on' ? 'https' : $scheme;
$httpHost = $_SERVER['HTTP_HOST'] ?? 'localhost';
$baseUrl = $protocol . '://' . $httpHost;
$isDevelopment = parse_url($baseUrl, PHP_URL_HOST) == 'localhost';

require_once($autoload);

$factory = new Cgi();
$factory
    ->withBaseUrl($baseUrl)
    ->withBaseCgi($baseCgi)
    ->withBaseApp($baseApp)
    ->withBaseView($baseApp . DIRECTORY_SEPARATOR . 'view')
    ->withBaseResource($baseApp . DIRECTORY_SEPARATOR . 'resource')
    ->withBaseRoute($baseApp . DIRECTORY_SEPARATOR . 'route')
    ->withSplAutoload('Control', $baseApp . DIRECTORY_SEPARATOR . 'control')
    ->withSplAutoload('Model', $baseApp . DIRECTORY_SEPARATOR . 'model')
    ->withEnvironment($environment)
    ->withErrorHandler(function (Throwable $throwable) use ($isDevelopment): ResponseInterface {
        $status = 500;
        $error = 'Internal Server Error';

        $exceptions = [
            ['instance' => NotFoundException::class, 'status' => 404, 'error' => 'Not Found'],
            ['instance' => MethodNotAllowedException::class, 'status' => 405, 'error' => 'Method Not Allowed'],
        ];

        foreach ($exceptions as $exception) {
            if ($throwable instanceof $exception['instance']) {
                $status = $exception['status'];
                $error = $exception['error'];
                break;
            }
        }

        $errorHandler = new ErrorHandler([
            'status' => $status,
            'error' => $error,
            'throwable' => $isDevelopment ? $throwable : null
        ]);

        return $errorHandler->getHtmlResponse();
    })
    ->run();
