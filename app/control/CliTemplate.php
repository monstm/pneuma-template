<?php

namespace App\Control;

use Pneuma\CliExtension;
use Pneuma\Interface\InputInterface;
use Pneuma\Interface\OutputInterface;

/**
 * Describes CliTemplate control.
 */
class CliTemplate extends CliExtension
{
    /**
     * commandMethod method control.
     *
     * @param InputInterface $input Console input interface
     * @param OutputInterface $output Console output interface
     * @return bool
     */
    public function commandMethod(InputInterface $input, OutputInterface $output): bool
    {
        return true;
    }
}
