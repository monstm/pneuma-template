<?php

namespace App\Control;

use Pneuma\CgiExtension;
use Pneuma\Interface\ResponseInterface;
use Pneuma\Interface\ServerRequestInterface;

/**
 * Describes CgiTemplate control
 */
class CgiTemplate extends CgiExtension
{
    /**
     * GET / fast route
     *
     * @param ServerRequestInterface $request PSR-7 ServerRequestInterface
     * @param ResponseInterface $response PSR-7 ResponseInterface
     * @param array<string,string> $arguments Fast route srguments
     * @return ResponseInterface
     */
    public function getCgiTemplate(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments
    ): ResponseInterface {
        $controlFile = realpath(__FILE__) . '(' . __LINE__ . '): ' . __FUNCTION__;
        $viewName = 'template.twig';

        return $response->html($this->view($viewName, array(
            'base_url' => $this->baseUrl(),

            'base_app' => $this->baseApp(),
            'base_cgi' => $this->baseCgi(),
            'control_file' => $controlFile,
            'view_file' => $this->baseView($viewName),

            'year' => date('Y')
        )));
    }
}
