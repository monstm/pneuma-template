<?php

use Pneuma\Command;
use Pneuma\Constant\ArgumentType;
use Pneuma\Constant\OptionType;

Command::add('command_name', 'App\Control\CliTemplate:commandMethod', [
    'description' => 'cli template description',
    'help' => 'cli template help',
    'arguments' => [
        [
            'name' => 'argument_mandatory',
            'type' => ArgumentType::MANDATORY,
            'description' => 'argument mandatory description'
        ],
        [
            'name' => 'argument_optional',
            'type' => ArgumentType::OPTIONAL,
            'description' => 'argument optional description',
            'default' => 'default value'
        ],
        [
            'name' => 'argument_array',
            'type' => ArgumentType::ARRAY,
            'description' => 'argument array description',
            'default' => ['default1', 'default2', 'default3']
        ],
    ],
    'options' => [
        [
            'name' => 'option_blank',
            'alias' => 'b',
            'type' => OptionType::BLANK,
            'description' => 'option blank description',
            'default' => 'option blank default'
        ],
        [
            'name' => 'option_value',
            'alias' => 'ov',
            'type' => OptionType::VALUE,
            'description' => 'option value description',
            'default' => 'option value default'
        ],
        [
            'name' => 'option_optional',
            'alias' => 'o',
            'type' => OptionType::OPTIONAL,
            'description' => 'option optional description',
            'default' => 'option optional default'
        ],
        [
            'name' => 'option_array',
            'alias' => 'a',
            'type' => OptionType::ARRAY,
            'description' => 'option array description',
            'default' => 'option array default'
        ],
        [
            'name' => 'option_reverse',
            'alias' => 'r',
            'type' => OptionType::REVERSE,
            'description' => 'option reverse description',
            'default' => 'option reverse default'
        ],
    ]
]);
