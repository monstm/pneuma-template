@echo off

set SERVER_HOST=localhost
set SERVER_PORT=8080
set CGI_PATH=cgi


set DOC_ROOT=%~dp0%CGI_PATH%
title %SERVER_HOST%:%SERVER_PORT% %DOC_ROOT%

echo WARNING!
echo This web server is designed to aid application development.
echo It may also be useful for testing purposes or for application demonstrations that are run in controlled environments.
echo It is not intended to be a full-featured web server. It should not be used on a public network.
echo.

php -S %SERVER_HOST%:%SERVER_PORT% -t %DOC_ROOT%
