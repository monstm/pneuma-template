<?php

use Pneuma\Cli;

$baseCli = __DIR__;
$root = dirname($baseCli);
$baseApp = $root . DIRECTORY_SEPARATOR . 'app';
$environment = $root . DIRECTORY_SEPARATOR . 'environment.ini';
$defaultAutoload = $root . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
$autoload = $GLOBALS['_composer_autoload_path'] ?? $defaultAutoload;

require_once($autoload);

$factory = new Cli();
$factory
    ->withBaseCli($baseCli)
    ->withBaseApp($baseApp)
    ->withBaseView($baseApp . DIRECTORY_SEPARATOR . 'view')
    ->withBaseResource($baseApp . DIRECTORY_SEPARATOR . 'resource')
    ->withBaseCommand($baseApp . DIRECTORY_SEPARATOR . 'command')
    ->withSplAutoload('Control', $baseApp . DIRECTORY_SEPARATOR . 'control')
    ->withSplAutoload('Model', $baseApp . DIRECTORY_SEPARATOR . 'model')
    ->withEnvironment($environment)
    ->run();
