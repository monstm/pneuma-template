FROM php:8-apache

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
    && a2enmod rewrite \
    && chown -R www-data:www-data /var/www \
    && chmod -R 0777 /var/www

WORKDIR /var/www
