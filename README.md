# Pneuma Template

## Composer update

> docker run --rm -v .:/app composer up

## Create and start containers

> docker-compose up -d --build

## Stop and remove containers, networks

> docker-compose down

## Execute a command in a running container

> docker exec -it pneuma-php /bin/bash

## Running local test

* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpcs --standard=PSR1,PSR2,PSR12 app cgi cli test
* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpstan analyse -c phpstan.neon
* docker run --rm -v .:/pneuma -w /pneuma php:cli-alpine php vendor/bin/phpunit
